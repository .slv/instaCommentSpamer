from time import sleep
import datetime
from selenium import webdriver
from selenium.webdriver import *
from selenium.webdriver.common.keys import Keys

#variables
user_email = '';
user_password = '';
comment_list = '';
instagram_link = ''

#code
options = webdriver.ChromeOptions();
options.add_argument('headless');
options.add_argument('window-size=1200x600')
browser = webdriver.Chrome(chrome_options=options)


#Log in
browser.get('https://www.instagram.com/accounts/login/');
sleep(2);

input = browser.find_elements_by_xpath('//form/div/div/div/input');
ActionChains(browser)\
    .move_to_element(input[0]).click()\
    .send_keys(user_email)\
    .perform();
ActionChains(browser)\
    .move_to_element(input[1]).click()\
    .send_keys(user_password)\
    .perform();

login_button = browser.find_element_by_xpath('//form/span/button[text()="Log in"]');
ActionChains(browser)\
    .move_to_element(login_button).click()\
    .perform();
sleep(2);

now = datetime.datetime.now()
print("["+str(now.day) + "|" + str(now.month) + " @ " + str(now.hour) + ":" + str(now.minute) + "]- Logged In!");

#Go to wanted link
browser.get(instagram_link);

#open file and start spamming, 10 per hour due to instagram limitation
fo = open(comment_list, "r+");
for line in fo:
    comment_box = browser.find_element_by_xpath('//form/textarea');
    ActionChains(browser)\
        .move_to_element(comment_box).click()\
        .send_keys(line)\
        .send_keys(Keys.RETURN)\
        .perform();
    now = datetime.datetime.now()
    print("["+str(now.day) + "|" + str(now.month) + " @ " + str(now.hour) + ":" + str(now.minute) + "]- Commented: " + line, end='');
    sleep(45);

now = datetime.datetime.now()
print("["+str(now.day) + "|" + str(now.month) + " @ " + str(now.hour) + ":" + str(now.minute) + "]- Job Done!");
fo.close();

browser.close();
